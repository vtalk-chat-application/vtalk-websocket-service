# stage 1 - creating a binary from a golang image
FROM golang:1.16-alpine AS builder

# since it is an alpine build update it.
RUN apk update

# set working directory - similar to cd in bash :D
WORKDIR /app

COPY . .

RUN go mod tidy

RUN go build -o bin/service main/main.go

# stage 2 - create a light weight container by copying only the compiled file from the stage 1 build.
FROM alpine

WORKDIR /app

COPY --from=builder /app/bin/service .

COPY --from=builder /app/config/ config/ 

EXPOSE 4447

CMD ["/app/service"]