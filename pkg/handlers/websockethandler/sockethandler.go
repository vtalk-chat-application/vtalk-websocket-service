package websockethandler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"vtalk-websocket-service/pkg/servicecall"
	"vtalk-websocket-service/pkg/utils"
)

type WebSocketContents struct {
	ctxt *utils.Context
}

func GetWebSocketContents(ctxt *utils.Context) *WebSocketContents {
	return &WebSocketContents{
		ctxt: ctxt,
	}
}

// serveWs handles websocket requests from the peer.
func (wc *WebSocketContents) serveWebSocketUserConnection(w http.ResponseWriter, r *http.Request) {

	query := r.URL.Query()
	auth, authOk := query["Auth"]
	if !authOk || auth[0] == "" {
		fmt.Print("ws:Err: Unauthorized: token Notfound")
		http.Error(w, "Err: Unauthorized", http.StatusUnauthorized)
		return
	}

	upgrader.CheckOrigin = func(r *http.Request) bool {
		// check origin
		return true
	}

	// create new websocket connection
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err.Error())
		return
	}

	c := &connection{send: make(chan []byte, 256), ws: ws}

	// veryfy auth
	authResp, userData := wc.ctxt.VerifyUserAuthentication(auth[0])
	if authResp.HttpStatus != http.StatusOK {
		fmt.Print("ws:Err: Unauthorized: Invalid token")
		errResp, _ := json.Marshal(map[string]interface{}{"error": "Unauthorized", "code": 401})
		c.send <- errResp
		close(c.send)
		return
	}

	s := subscription{c, userData.UserID, wc}
	wsHubStore.register(s)

	go s.writePump()
	go s.readPump()
}

func (wc *WebSocketContents) serveWebSocketDistributeChat(w http.ResponseWriter, r *http.Request) {
	// verify service token

	var response servicecall.Response
	// ctxt := r.Context()
	requestData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("serveWebSocketDistributeChat: ReadAll failed", err.Error())
		response.WriteError(w, http.StatusBadRequest, servicecall.ErrBadRequest, "Input missing")
		return
	}

	m := Message{data: requestData}
	wsHubStore.handleEventMessage(m, wc)
}
