package websockethandler

import (
	"net/http"

	"github.com/gorilla/mux"
)

func (wc *WebSocketContents) AddHandler(router *mux.Router) {

	//public api to connect room
	router.HandleFunc("/ws/connect/user/room", wc.serveWebSocketUserConnection).Methods(http.MethodGet)

	wc.AddUserHandlerV1(router.PathPrefix("/service/api/v1/ws").Subrouter())

}

// internal api
func (wc *WebSocketContents) AddUserHandlerV1(wsRouter *mux.Router) {

	wsRouter.HandleFunc("/broadcast/chat", wc.ctxt.ValidateServiceToken(wc.serveWebSocketDistributeChat)).Methods(http.MethodPost)
}
