package websockethandler

import (
	"encoding/json"
	"vtalk-websocket-service/pkg/model"
)

type Message struct {
	data []byte
	room string
}

type roomModel struct {
	connection *connection
	status     bool
}

type wsHub struct {
	rooms map[string]roomModel
}

// store all websocket connections.
var wsHubStore = wsHub{
	rooms: make(map[string]roomModel),
}

func (wh *wsHub) register(s subscription) {
	if s.conn == nil {
		return
	}
	var newRoom = roomModel{
		connection: s.conn,
		status:     true,
	}

	wh.rooms[s.room] = newRoom
}
func (wh *wsHub) unregister(s subscription) {
	delete(wh.rooms, s.room) // TODO -- Some crash is here check what it is ?!!! ----
	close(s.conn.send)
}

func (wh *wsHub) handleEventMessage(m Message, wc *WebSocketContents) {
	var neWsEvent model.EventBody
	if err := json.Unmarshal(m.data, &neWsEvent); err != nil {
		return
	}

	// Get the required function for the event action type from the messageEvents map.
	if value, ok := messageEvents[neWsEvent.Action]; ok {

		// Since we the the value as an interface, we need to parse it to a function
		if messageFunc, mfOk := value.(func(model.EventBody, Message, *WebSocketContents)); mfOk {

			// If we successfully get the function we call it by the required arguments
			messageFunc(neWsEvent, m, wc)
		}
	}
}
