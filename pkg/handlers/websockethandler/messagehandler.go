package websockethandler

import (
	"encoding/json"
	"vtalk-websocket-service/pkg/model"
)

// add all functions
var messageEvents = map[string]interface{}{
	// "chat_msg":   messageBroadcast,
	"broad_chat": broadcastChat,
}

// func messageBroadcast(body model.EventBody, m Message, wc *WebSocketContents) {
// 	tempByte, err := json.Marshal(body.Data)
// 	if err != nil {
// 		return
// 	}
// 	var chatMsg model.MessageModel
// 	if err = json.Unmarshal(tempByte, &chatMsg); err != nil {
// 		return
// 	}

// 	fmt.Println("direct messge: ", string(m.data))

// 	// verify and get chat audience to broad cast messages.
// 	chatResp, chatData := wc.ctxt.VerifyUserChatId(m.room, chatMsg.ChatID)
// 	if chatResp.HttpStatus != http.StatusOK {
// 		fmt.Print("Err: Unauthorized: Invalid chatid")
// 		return
// 	}
// 	for _, recieveUser := range chatData.Audience {
// 		if chatMsg.CreatedBy != recieveUser {
// 			reciverRoom, ok := wsHubStore.rooms[recieveUser]
// 			if !ok || reciverRoom.connection == nil {
// 				continue
// 			}
// 			reciverRoom.connection.send <- m.data
// 		}
// 	}
// }

func broadcastChat(body model.EventBody, m Message, wc *WebSocketContents) {
	tempByte, err := json.Marshal(body.Data)
	if err != nil {
		return
	}
	var chatMsg model.BroadcastMessageData
	if err = json.Unmarshal(tempByte, &chatMsg); err != nil {
		return
	}

	eventBody, err := json.Marshal(map[string]interface{}{"action": body.Action, "data": chatMsg.Message})
	if err != nil {
		return
	}

	for _, recieveUser := range chatMsg.ChatData.Audience {
		if chatMsg.Message.CreatedBy != recieveUser {
			reciverRoom, ok := wsHubStore.rooms[recieveUser]
			if !ok || reciverRoom.connection == nil {
				continue
			}
			reciverRoom.connection.send <- eventBody
		}
	}
}
