package servicecall

import (
	"io"
	"net/http"

	"github.com/golang-jwt/jwt"
)

// Service Request model
type Request struct {
	Path               string
	Method             string
	Header             http.Header
	PayloadReadCloser  io.Reader
	PayloadByte        []byte
	BaseURL            string
	AccessTokenSecrect string
}

// General claims model
type Claims struct {
	jwt.StandardClaims
}

// Service response Model
type ResponseError struct {
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

type Response struct {
	ResponseError ResponseError `json:"error"`
	Message       string        `json:"message"`
	Result        interface{}   `json:"result"`
	HttpStatus    int64         `json:"http_status"`
}
