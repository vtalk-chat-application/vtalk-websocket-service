# vTalk Websocket Service
- This service works as the web socket.
- Start this service by running the __run.sh__ script
> ./run.sh
- the update config file is used to update details on the production build
 ### Docker
 - update the config details of service address and db connection string by using docker volume. by creating a 
 __updateconfig/config.json__ file in the config folder.

 - use __host.docker.internal__ insted of __localhost__ if you want to run the docke in local machine. to establish conection to host db or port from container.
 #### Docker build command
 > docker build -t vtalk-websocket-version .
 - use __--no-cache__ to avoid buid cashing if needed.
 #### Docker run command
 > docker run -d -p 4447:4447  --name vtalk-websocket-container-version vtalk-websocket-version
 #### Generic run cammand with volume
 > docker run -d -v HOST_FOLDER_LOCATION:CONTAINER_FOLDER_LOCATION:ro -p HOST_PORT:CONTAINER_PORT --name CONTAINER_NAME IMAGE_NAME